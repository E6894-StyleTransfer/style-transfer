from keras.models import *
from final_arch_utils import *

from bible_utils import *
import word2vec_utils as w2v

import numpy as np

from content_pretrain import get_sentences, get_data

bible_map = get_bible_map()
w2v_model = w2v.initialize()

style_transfer = load_model("content_weights.h5")

sentences = get_sentences()
X, test_y = get_data(sentences)

print np.shape(sentences)
print np.shape(X)
print np.shape(test_y)

gen_y = style_transfer.predict(X)

for g_ys, ys in zip(gen_y, sentences):
#    gen_y = style_transfer.predict([c, s])
    for i in range(6):
        g_y = g_ys[i]
        y = ys[i]

        gen_text = []
        src_text = []

        print("generated sentence")
        for gen_word in g_y:
            if gen_word[-1] == 1:
                print("NULL")
                gen_text.append("NULL")
            else:
                w = (w2v_model.similar_by_vector(gen_word[:-1]))[0]
                #            print(w[0])
                gen_text.append(w[0])

        print("\ntarget sentence")
        for word in y:
            if word[-1] == 1:
                print("NULL")
                src_text.append("NULL")
            else:
                w = (w2v_model.similar_by_vector(word[:-1]))[0]
                #            print(w[0])
                src_text.append(w[0])

        for w1, w2 in zip(gen_text, src_text):
            if w1 != "NULL" and w2 != "NULL":
                print(w2v_model.similarity(w1, w2))
