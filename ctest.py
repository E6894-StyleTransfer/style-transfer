from keras.models import *
from final_arch_utils import *

from bible_utils import *
import word2vec_utils as w2v
import numpy as np

bible_map = get_bible_map()
w2v_model = w2v.initialize()
w2v.unvectorize_initialize()

test_c, test_s, test_y = get_training_data(bible_map, w2v_model, 10)

input = Input((verse_length, dim))

# names content_1, content_2, content_3
#content_encoder = LSTM(latent_dim, return_sequences=True, dropout=0.15, recurrent_dropout=0.15, name='content_1')(input)
#content_encoder = BatchNormalization()(content_encoder)
#content_encoder = LSTM(latent_dim, return_sequences=True, dropout=0.15, recurrent_dropout=0.15, name='content_2')(content_encoder)
#content_encoder = BatchNormalization()(content_encoder)
#content_encoder = LSTM(latent_dim, return_sequences=True, dropout=0.15, recurrent_dropout=0.15, name='content_3')(content_encoder)
#content_encoder = BatchNormalization()(content_encoder)

content_encoder = Bidirectional(LSTM(latent_dim, return_sequences=True, dropout=0.15, recurrent_dropout=0.15, name='contet_bi_1'))(input)
content_encoder = LSTM(latent_dim, return_sequences=True, dropout=0.15, recurrent_dropout=0.15, name='content_2')(content_encoder)
content_encoder = LSTM(latent_dim, return_sequences=True, dropout=0.15, recurrent_dropout=0.15, name='content_3')(content_encoder)
content_encoder = LSTM(latent_dim, return_sequences=True, dropout=0.15, recurrent_dropout=0.15, name='content_4')(content_encoder)
content_encoder = BatchNormalization()(content_encoder)

#content_encoder = RepeatVector(verse_length)(content_encoder)
BBE_dec = LSTM(latent_dim, dropout=0.4, recurrent_dropout=0.2, return_sequences=True)(content_encoder)

ESV_dec = LSTM(latent_dim, dropout=0.4, recurrent_dropout=0.2, return_sequences=True)(content_encoder)

KJV_dec = LSTM(latent_dim, dropout=0.4, recurrent_dropout=0.2, return_sequences=True)(content_encoder)

MSG_dec = LSTM(latent_dim, dropout=0.4, recurrent_dropout=0.2, return_sequences=True)(content_encoder)

NIV_dec = LSTM(latent_dim, dropout=0.4, recurrent_dropout=0.2, return_sequences=True)(content_encoder)

NLT_dec = LSTM(latent_dim, dropout=0.4, recurrent_dropout=0.2, return_sequences=True)(content_encoder)

YLT_dec = LSTM(latent_dim, dropout=0.4, recurrent_dropout=0.2, return_sequences=True)(content_encoder)


pretrainer = Model(inputs=[input],
                   outputs=[BBE_dec, ESV_dec, KJV_dec, MSG_dec, NIV_dec, NLT_dec, YLT_dec])

pretrainer.load_weights("content_weights.h5")

gen_y = style_transfer.predict([test_c, test_s])

for i in range(0, len(gen_y)):
    print "sentence", i
    print "target:", w2v.unvectorize_sentence(test_y[i])
    print "generated:", w2v.unvectorize_sentence(gen_y[i])
    print "\n\n"
