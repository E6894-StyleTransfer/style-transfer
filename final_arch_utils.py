import random
import word2vec_utils as w2v
import numpy as np

max_verse_length = 50
max_style_length = 150

# return: train_c (training content), train_s (training style samples), train_y (synthesized)
def get_training_data(bible_map, w2v_model, n, style_length=150, style_chunk = 5):
    train_c = [] # content verses
    train_s = [] # style samples
    train_y = [] # same verse in the new style

    w2v.reset_global_unknown_vectors()

    while len(train_c) < n:
        # pick two random styles
        s1 = random.choice(sorted(bible_map.keys()))
        s2 = random.choice(sorted(bible_map.keys()))
#		while s2 == s1: # now we can also learn that v1 + s1 = v1 if we let style1 = style2
#			s2 = random.choice(sorted(bible_map.keys()))
        bible_s1 = bible_map[s1]
        bible_s2 = bible_map[s2]

		# pick a random verse and ensure it is in both bibles
        book_title = random.choice(sorted(bible_s1.keys()))
        if book_title not in bible_s2:
            continue
        book_s1 = bible_s1[book_title]
        book_s2 = bible_s2[book_title]
        if book_s1 is None or book_s2 is None:
            continue
        chapter_title = random.choice(sorted(book_s1.keys()))
        if chapter_title not in book_s2:
            continue
        chapter_s1 = book_s1[chapter_title]
        chapter_s2 = book_s2[chapter_title]
        if chapter_s1 is None or chapter_s2 is None:
            continue
        verse = random.choice(sorted(chapter_s1.keys()))
        if verse not in chapter_s2:
            continue
        verse_s1 = chapter_s1[verse]
        verse_s2 = chapter_s2[verse]
        if verse_s1 is None or verse_s2 is None:
            continue

        # get a style sample from the second version
        style_book = bible_s2[random.choice(sorted(bible_s2.keys()))]
        style_chapter = style_book[random.choice(sorted(style_book.keys()))]
        while len(style_chapter.keys()) < style_chunk:
            style_book = bible_s2[random.choice(sorted(bible_s2.keys()))]
            style_chapter = style_book[random.choice(sorted(style_book.keys()))]

        # from style_encoder_utils.py
        verse_start = random.choice( sorted( map( lambda x: int(x), style_chapter.keys() ) )[:-style_chunk+1] )
        style_strs = []
        for i in range(style_chunk):
            key = str(verse_start+i)
            if key in style_chapter and style_chapter[key] is not None:
                style_strs.append(style_chapter[key])
        style_str = " ".join(style_strs)

        #print("adding a new example")
        content_1 = w2v.vectorize(w2v_model, verse_s1.encode("ascii", "ignore"), pad_length=max_verse_length)
        content_2 = w2v.vectorize(w2v_model, verse_s2.encode("ascii", "ignore"), pad_length=max_verse_length)
        style_2 = w2v.vectorize(w2v_model, style_str.encode("ascii", "ignore"), pad_length=max_style_length)

        if (len(content_1) > max_verse_length or len(content_2) > max_verse_length or len(style_2) > max_style_length):
            continue

        train_c.append(content_1)
        train_s.append(style_2)
        train_y.append(content_2)

    w2v.save_global_unknown_vectors()

    return np.array(train_c), np.array(train_s), np.array(train_y)
