import random
import word2vec_utils as w2v
import numpy as np
import re

def get_style_training_pair_bible(bible_map,num_verses = 5):
    '''takes a map of the six flavors of bible and returns a tuple. 
       a string of num_verses concatenated consecutive versus 
       a numpy array indicated which bible version the the verses are from '''
    versions = sorted(bible_map.keys())
    bible_version = random.choice(versions)
    bible = bible_map[bible_version]
    
    #Ensure the chapter has at least num_verses in it`
    chapter = {}
    while( len(chapter) < num_verses ):
        book_title = random.choice(bible.keys())
        book = bible[book_title]
        chapter_title = random.choice(book.keys())
        chapter = book[chapter_title]
    
    verse_start = random.choice( sorted( map( lambda x: int(x), chapter.keys() ) )[:-num_verses+1] )
    style_strings = []
    for i in range(num_verses):
        key = str(verse_start+i)
        #Some versions of the bible omit or translate around specific verses
        if key in chapter and chapter[key] is not None:
            style_strings.append(chapter[key])
    style_string = " ".join(style_strings)
    
    #Generate Version Vector
    version_vector = [0]*len(versions)
    version_vector[versions.index(bible_version)] = 1

    return style_string.encode('ascii','ignore'), version_vector

def get_style_training_pair_other(sources,input_max_length):
    '''takes a list of lists of lines from any style source. 
       returns a string of num_verses concatedned consecutive lines
       a numpy array indicated which alternative source version the the verses are from '''
    source_version = random.choice(range(len(sources)))
    source = sources[source_version]
   
    start = random.choice( range(len(source) - 5 ) )
    style_string = "";
    next_line = re.sub(r"([\w/'+$\s]+|[^\w/'+$\s])\s*", r" \1", source[start] )
    while( len( style_string.split(" ") ) + len( next_line.split(" " ) ) < input_max_length 
           and start + 1< len(source)):
        style_string = style_string + " " + next_line
        start += 1
        next_line = source[start ]
        next_line = re.sub(r"([\w/'+$\s]+|[^\w/'+$\s])\s*", r" \1", next_line )

    #style_strings = source[start:start+num_verses]
    #style_string = " ".join(style_strings)
    
    #Generate Version Vector
    version_vector = [0]*len(sources)
    version_vector[source_version] = 1

    return style_string.encode('ascii','ignore'), version_vector

def get_n_encoded_training_pairs( bible_map, alternative_sources, w2v_model, n_pairs, input_max_length = 150, num_verses = 5 ):
    verses = []
    objectives = []
    examined = 0
    n_bibles = len(bible_map)
    n_other  = len(alternative_sources)
    bible_threshold = float(n_bibles)/(n_bibles + n_other)
    print(bible_threshold )
    bible_examined = 0
    w2v.reset_global_unknown_vectors()
    while len(objectives) < n_pairs:
        examined += 1
        if random.random() < bible_threshold:
            style_string,version_vector = get_style_training_pair_bible( bible_map, num_verses = num_verses )
            version_vector = np.array( version_vector + [0]*n_other)
            bible_examined += 1
        else:
            style_string,version_vector = get_style_training_pair_other( alternative_sources, input_max_length )
            version_vector = np.array( [0] * n_bibles + version_vector)
        style_vector = w2v.vectorize( w2v_model, style_string, pad_length = input_max_length )
        if len(style_vector) > input_max_length:
            continue
        verses.append(style_vector)
        objectives.append(version_vector)
    w2v.save_global_unknown_vectors()
    print( "percent bible content", 100 * bible_examined/examined)
    print( "examined", examined, "and kept percent", 100*n_pairs/examined )
    return( np.array( verses ), np.array(objectives ) )
