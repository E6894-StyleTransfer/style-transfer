import keras
from gensim.models import Word2Vec
from bible_utils import *
import word2vec_utils as w2v
from keras.preprocessing import sequence
from keras.models import Sequential, Model
from keras.layers import Input, LSTM, Dense, Dropout, Activation, Embedding, TimeDistributed
import numpy as np
from style_encoder_utils import *
from time import time

bible_map = get_bible_map()
num_bible_versions = len(bible_map)
w2v_model = w2v.initialize()

const_my = json.load( open( 'data/' + 'constitution' + '.json' ) )
taylor   = json.load( open( 'data/' + 'Taylor' +  '.json'))
eminem   = json.load( open( 'data/' + 'Eminem' +  '.json'))
movies   = json.load( open( 'data/' + 'Movies' +  '.json'))
aux_sources = [const_my,taylor,eminem,movies]

start = time()

style_vects, objectives = get_n_encoded_training_pairs( bible_map, aux_sources,w2v_model, 20000, input_max_length = input_max_length )
end = time()
per_entry = (end - start ) / len(objectives)
print( per_entry )

data_dim = len(style_vects[0][0])
output_size = len(objectives[0])
dropout_level = 0.15

styleInput = Input(shape=(input_max_length,data_dim)) # 150 size 301 word vectors

# define style model
a = LSTM(data_dim-51, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_1")
b = LSTM(data_dim-151, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_2")
c = LSTM(data_dim-201, return_sequences = False, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_3")
d = Dropout(0.7,name='style_throwaway_0')
e = Dense(output_size, activation='softmax',use_bias=False,name='style_throwaway_a')
a.trainable = True
b.trainable = True
c.trainable = True

style = e(d(c(b(a(styleInput)))))
# load style model's weights from pre-training
model = Model(styleInput, style)

rms = keras.optimizers.RMSprop(lr = 0.00055)
model.compile(loss = 'categorical_crossentropy',
              optimizer=rms,
              metrics=['accuracy']
              )
model.load_weights('style_weights.h5',by_name = True )
model.fit(style_vects, objectives,
          batch_size=250, epochs=5,
          validation_split=0.05
         )
model.save('style_weights.h5')
test_size = len(style_vects)//20
test_style_vects, test_objectives = get_n_encoded_training_pairs( bible_map, aux_sources,w2v_model, test_size, input_max_length = input_max_length )
score = model.evaluate(test_style_vects, test_objectives, batch_size=64)
print("\nModel Accuracy: %.2f%%" % (score[1]*100))