from keras.layers import Input, LSTM, RepeatVector, concatenate
from keras.layers.wrappers import Bidirectional
from keras.models import Model, Sequential
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from final_arch_utils import *

from bible_utils import *
import word2vec_utils as w2v


K.set_learning_phase(1)
K._LEARNING_PHASE = K.constant(1)

bible_map = get_bible_map()
w2v_model = w2v.initialize()

# size definitions
embedding_size = 301
latent_dim = 301
content_max_length = 50
style_max_length = 150
output_size = 50

nb_epoch = 8
train_flag = False
dropout_level = 0.15

def sched(epoch):
    if epoch < 2:
        return 0.001
    elif epoch < 5:
        return 0.0007
    else:
        return 0.0005


# define shapes of the inputs
contentInput = Input(shape=(content_max_length, embedding_size)) # 50 size 301 word vectors
styleInput = Input(shape=(style_max_length, embedding_size)) # 150 size 301 word vectors

# define content model
content_l1 = Bidirectional(LSTM(latent_dim, return_sequences = True, trainable=train_flag,dropout=dropout_level, recurrent_dropout=dropout_level, name="content_bi_1"))
content_l2 = LSTM(latent_dim, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="content_2")
content_l3 = LSTM(latent_dim, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="content_3")
content_l4 = LSTM(latent_dim, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="content_4")

# freeze content weights for first few epochs
content_l2.trainable = train_flag
content_l3.trainable = train_flag
content_l4.trainable = train_flag

content = content_l4(content_l3(content_l2(content_l1(contentInput))))

# load content model's weights from pre-training
content_model = Model(contentInput, content)
content_model.load_weights("content_weights.h5", by_name=True)

# define style model
style_l1 = LSTM(latent_dim-51, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_1")
style_l2 = LSTM(latent_dim-151, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_2")
style_l3 = LSTM(latent_dim-201, return_sequences = False, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_3")

# freeze style weights for first few epochs
style_l1.trainable = train_flag
style_l2.trainable = train_flag
style_l3.trainable = train_flag

style = style_l3(style_l2(style_l1(styleInput)))

# load style model's weights from pre-training
style_model = Model(styleInput, style)
style_model.load_weights("style_weights.h5", by_name=True)

# define final style transfer model
style = RepeatVector(output_size, name="style_repeat_1")(style)
latent = concatenate([content, style])
synthesized = LSTM(latent_dim+100, return_sequences = True)(latent)
synthesized = LSTM(latent_dim, return_sequences = True)(synthesized)
synthesized = LSTM(latent_dim, return_sequences = True)(synthesized)

# final model
style_transfer = Model(inputs=[contentInput, styleInput], outputs=[synthesized])

#style_transfer.load_weights("style_transfer_recurrent_weights.h5")

style_transfer.compile(optimizer='rmsprop', loss="cosine_proximity", metrics=["accuracy"])

checkpointer = ModelCheckpoint('style_transfer_best.h5', verbose=1, monitor='val_acc', save_best_only=True)
lrs = LearningRateScheduler(sched)

style_transfer.summary()

#test_c, test_s, test_y = get_training_data(bible_map, w2v_model, 10)
#gen_y = style_transfer.predict([test_c, test_s])

# load training data
train_c, train_s, train_y = get_training_data(bible_map, w2v_model, 1000)
print "finished creating dataset 1"
style_transfer.fit([train_c, train_s], train_y, validation_split=0.05, epochs=nb_epoch, callbacks=[checkpointer, lrs])
style_transfer.save("style_transfer_final.h5")

finished_sets = 0
tries = 0
while finished_sets < 2 and tries < 20:
	tries += 1
	try:
		train_c, train_s, train_y = get_training_data(bible_map, w2v_model, 5000)
		print "finished creating dataset", (finished_sets + 1)
		style_transfer.fit([train_c, train_s], train_y, validation_split=0.05, epochs=nb_epoch, callbacks=[checkpointer, lrs])
		style_transfer.save("style_transfer_final.h5")
		finished_sets += 1
	except Exception:
		continue

#test_c, test_s, test_y = get_training_data(bible_map, w2v_model, 1000)
# test the model
#style_transfer.evaluate([test_c, test_s], test_y)
#style_transfer.save("style_transfer_final.h5")

# -------------------------------------------------------------------------------------------------------------------------
# ENTIRE MODEL trainable
train_flag = True

ontentInput = Input(shape=(content_max_length, embedding_size)) # 50 size 301 word vectors
styleInput = Input(shape=(style_max_length, embedding_size)) # 150 size 301 word vectors

# define content model
content_l1 = Bidirectional(LSTM(latent_dim, return_sequences = True, trainable=train_flag,dropout=dropout_level, recurrent_dropout=dropout_level, name="content_bi_1"))
content_l2 = LSTM(latent_dim, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="content_2")
content_l3 = LSTM(latent_dim, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="content_3")
content_l4 = LSTM(latent_dim, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="content_4")

# freeze content weights for first few epochs
content_l2.trainable = train_flag
content_l3.trainable = train_flag
content_l4.trainable = train_flag

content = content_l4(content_l3(content_l2(content_l1(contentInput))))

# load content model's weights from pre-training
content_model = Model(contentInput, content)

# define style model
style_l1 = LSTM(latent_dim-51, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_1")
style_l2 = LSTM(latent_dim-151, return_sequences = True, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_2")
style_l3 = LSTM(latent_dim-201, return_sequences = False, dropout=dropout_level, recurrent_dropout=dropout_level, name="style_3")

# freeze style weights for first few epochs
style_l1.trainable = train_flag
style_l2.trainable = train_flag
style_l3.trainable = train_flag

style = style_l3(style_l2(style_l1(styleInput)))

# load style model's weights from pre-training
style_model = Model(styleInput, style)

# define final style transfer model
style = RepeatVector(output_size, name="style_repeat_1")(style)
latent = concatenate([content, style])
synthesized = LSTM(latent_dim+100, return_sequences = True)(latent)
synthesized = LSTM(latent_dim, return_sequences = True)(synthesized)
synthesized = LSTM(latent_dim, return_sequences = True)(synthesized)

# final model
style_transfer = Model(inputs=[contentInput, styleInput], outputs=[synthesized])

style_transfer.load_weights("style_transfer_best.h5")

style_transfer.compile(optimizer='rmsprop', loss="cosine_proximity", metrics=["accuracy"])

checkpointer = ModelCheckpoint('style_transfer_best_unchained.h5', verbose=1, monitor='val_acc', save_best_only=True)
lrs = LearningRateScheduler(sched)

style_transfer.summary()

#test_c, test_s, test_y = get_training_data(bible_map, w2v_model, 10)
#gen_y = style_transfer.predict([test_c, test_s])

# load training data
for i in range(0,3):
	try:
		train_c, train_s, train_y = get_training_data(bible_map, w2v_model, 5000)
		print "finished creating dataset", i
		style_transfer.fit([train_c, train_s], train_y, validation_split=0.05, epochs=nb_epoch, callbacks=[checkpointer, lrs])
		style_transfer.save("style_transfer_final_unchained.h5")
	except Exception:
		continue