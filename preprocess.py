import pickle

from bible_utils import get_verse, get_bible_map

versions = ['ESV', 'MSG', 'BBE', 'YLT', 'KJV', 'NLT', 'NIV']
data_folder = 'data'

splits = 5 # how many sections to split the data into

bible_map = get_bible_map()

verses = []

for book in bible_map['KJV']:
    for chapter in bible_map['KJV'][book]:
        for verse in bible_map['KJV'][book][chapter]:
            try:
                v = get_verse(book, chapter, verse)
                verses.append([book, chapter, verse])
            except:
                continue

split_verses = []

s = len(verses)//splits

for i in range(splits):
    split_verses.append(verses[i * s : (i + 1) * s])

split_verses[-1].append(verses[(i + 1) * s :])

pickle.dump(split_verses, open('verses_in_' + str(splits) + '.pkl', 'w'))
