import gensim
import re
import numpy as np
import json
import pickle
import math

from gensim.models import Word2Vec

word2vec_path = './GoogleNews-vectors-negative300.bin'
unknown_path  = 'unknown_words_stored.pkl'

def initialize():
    w2v_model = gensim.models.KeyedVectors.load_word2vec_format(word2vec_path, binary=True)
    return( w2v_model ) 

def get_unknown_vectors():
    with open(unknown_path, 'rb') as f:
        unknown_vectors = pickle.load(f)
    return( unknown_vectors )

def save_unknown_vectors(vecs):
    with open(unknown_path, 'wb') as f:
        pickle.dump(vecs, f)

def unvectorize_initialize():
    reset_global_unknown_vectors()
    initialize_global_w2v_model()
    
def unvectorize_sentence( sentence ):
    ''' Takes a numpy array indicating a sentence where each entry is a 301 dim
        vector indicating the predicted embedding of a word.
        
        returns a single string representing the sentence
    '''
    words = []
    for word in sentence:
        words.append( unvectorize_word( word ) )
    return( " ".join(words) )

def unvectorize_word( word ):
    ''' Takes a numpy array inidicating a word embedid in 301 dim space and 
        returns the most likely word
    '''
    if word[-1] == 1: #Parse NULL predicted words
        ret = "_"
    else:
        # Get w2v best word and similarity score
        word = word[:-1]
        word_sum_square = np.sum(word**2) # calculate once here to avoid multiple calculation
        
        #w2v_word,w2v_similarity 
        temp = global_w2v_model.similar_by_vector( word, topn = 2 )
        if temp[0][0] == 'the':
            w2v_word,w2v_similarity = temp[1]
        else:
            w2v_word,w2v_similarity = temp[0]
        

        #Very confident it is this word
        if w2v_similarity > 0.999:
            ret = w2v_word
        else:
            #Unknown word 
            uk_word,uk_similarity = get_best_unknown_word( word, word_sum_square )
            print( "w2v word-score", w2v_word, w2v_similarity, "\n", "uk word-score", uk_word, uk_similarity )
            if w2v_similarity > uk_similarity:
                ret = w2v_word
            else:
                ret = uk_word
            
    return ret
        
def get_best_unknown_word( word, word_sum_square ):
    global unknown_vectors
    best_similarity_so_far = 0
    best_word_so_far = "_"
    for uk_word in unknown_vectors:
        uk_embedding = unknown_vectors[uk_word][:-1]#remove null dimension
        similarity = get_similarity( uk_embedding, word, word_sum_square )
        if similarity > best_similarity_so_far:
            best_similarity_so_far = similarity
            best_word_so_far = uk_word
            
    return( best_word_so_far, best_similarity_so_far )
        
def get_similarity( w1,w2,sumyy ):
    sumxx = np.sum(w1**2 )
    sumxy = np.sum(w1 * w2 )
    ret = sumxy/math.sqrt(sumxx*sumyy)
    return( ret )
# input: trained model, one sentence (string)
# output: a list of word2vec vectors for that sentence

def vectorize(model, sentence, pad_length = -1 ):
    global unknown_vectors 
    global global_w2v_model

    if model is None:
        print("Attempting to use global model")
        model = global_w2v_model
    
    #If we are passed unknown vectors then dont load or save it down;
    # New vectors will be stored in variable as sideffect and saved by calling function
    #passed_unknown_vectors = False
    #if( len( unknown_vectors ) == 0 ):
    #    passed_unknown_ectors = True
    #    unknown_vectors = get_unknown_vectors() 
        
    model_dimension = len(model['the'])#Assume the to always be in the model

    sentence = sentence.encode('ascii', 'ignore')
    sentence = re.sub(r"([\w/'+$\s]+|[^\w/'+$\s])\s*", r" \1", sentence)
    words = str.split(sentence.strip(), " ")
    vectorized_sentence = []

    #should_save_unknown_vectors = False
    for word in words:
        lower_word = word.lower()
        if word in model:
            vectorized_sentence.append(np.append(model[word],0))
        elif lower_word in model:
            vectorized_sentence.append(np.append(model[lower_word],0))
        elif lower_word in unknown_vectors:
            vectorized_sentence.append(unknown_vectors[lower_word])
        else:
            # Yoon Kim's unknown word handling, 2014
            # https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py#L88
            unknown_vectors[lower_word] = np.append(np.random.uniform(-0.25,0.25,300),0)
            vectorized_sentence.append(unknown_vectors[lower_word])
            #should_save_unknown_vectors = True
            
    if( pad_length != -1 ):
        while( len(vectorized_sentence) < pad_length ):
            vectorized_sentence.append(unknown_vectors['__BLANK__'])

    #if( save_unknown_vectors and not passed_unknown_vectors ):
    #    save_unknown_vectors(unknown_vectors)
            
    return np.array(vectorized_sentence)

def reset_global_unknown_vectors():
    global unknown_vectors 
    unknown_vectors = get_unknown_vectors()

def save_global_unknown_vectors():
    global unknown_vectors 
    save_unknown_vectors(unknown_vectors)

def initialize_global_w2v_model():
    global global_w2v_model
    global_w2v_model = initialize()
    

#model = initialize()
#print vectorize(model, "Hello, this is a test!")

def train(sentences, outf, size=100, win=11, mcount=3, wkrs=4):
    model = Word2Vec(sentences, size=size, window=win,
                     min_count=mcount, workers=wkrs)
    model.save(outf)

    return model

def train_on_files(files, outf, size=100, win=11, mcount=3, wkrs=4,
                   verbose=True):
    sentences = []

    if verbose:
        print('Reading sentences')
    
    for f in files:
        if verbose:
            print('Reading from file ' + f)
        
        trans = json.loads(open(f, 'r').read())

        for book in trans:
            for chapter in trans[book]:
                for verse in trans[book][chapter]:
                    s = []

                    v = trans[book][chapter][verse]
                    if v == None:
                        continue

                    for c in v:
                        if c.isalpha():
                            s.append(c.lower())

                        else:
                            s.append(' ')
                            s.append(c)

                    sentence = ''

                    for c in s:
                        sentence = sentence + c

                    sentence = sentence.encode('ascii', 'ignore')
                        
                    sentence = sentence.split()

                    sentences.append(sentence)

    if verbose:
        print('Training...')
                    
    return train(sentences, outf, size=size, win=win, mcount=mcount, wkrs=wkrs)
