
# coding: utf-8

# In[1]:

import keras
from gensim.models import Word2Vec
from bible_utils import *
import word2vec_utils as w2v
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Activation, Embedding, TimeDistributed
import numpy as np
from style_encoder_utils import *
from time import time


# In[2]:

bible_map = get_bible_map()
num_bible_versions = len(bible_map)
w2v_model = w2v.initialize()
my_verse = get_verse( "John",3,16)
output_map = dict()
for i,version in enumerate( bible_map.keys() ):
    target_array = np.zeros( num_bible_versions )
    target_array[i] = 1
    output_map[version] = target_array


# In[3]:

const_my = json.load( open( 'data/' + 'constitution' + '.json' ) )


# In[4]:

input_max_length = 150


# In[19]:

start = time()

style_vects, objectives = get_n_encoded_training_pairs( bible_map, [const_my],w2v_model, 2500, input_max_length = input_max_length )
end = time()
per_entry = (end - start ) / len(objectives)
print( per_entry )
len(w2v.get_unknown_vectors())


# In[ ]:

data_dim = len(style_vects[0][0])
output_size = len(objectives[0])
dropout_level = 0.15
model = Sequential()
model.add(LSTM(data_dim,input_shape=(input_max_length,data_dim),return_sequences=True, 
              recurrent_dropout=dropout_level, dropout=dropout_level,name='style_1'))
model.add(LSTM(data_dim-101,return_sequences=True, recurrent_dropout=dropout_level, dropout=dropout_level,name='style_2'))
model.add(LSTM(data_dim-201,return_sequences=False, recurrent_dropout=dropout_level, dropout=dropout_level,name='style_3'))

model.add(Dropout(0.4,name='style_throwaway_0'))
model.add(Dense(output_size, activation='softmax',use_bias=False,name='style_throwaway_1'))
rms = keras.optimizers.RMSprop(lr = 0.001)
model.compile(loss = 'categorical_crossentropy',
              optimizer=rms,
              metrics=['accuracy']
              )


# In[ ]:

model.load_weights('style_weights.h5')
model.fit(style_vects, objectives,
          batch_size=128, epochs=3,
          validation_split=0.05
         )
model.save('style_weights.h5')
test_size = len(style_vects)//20
test_style_vects, test_objectives = get_n_encoded_training_pairs( bible_map, [const_my],w2v_model, test_size, input_max_length = input_max_length )
score = model.evaluate(test_style_vects, test_objectives, batch_size=64)
print("\nModel Accuracy: %.2f%%" % (score[1]*100))


# In[8]:

model.summary()
#model.save('style_weights.h5')


# In[ ]:

data_dim


# In[5]:

import pickle 
unknown_path  = 'unknown_words_stored.pkl'

def get_unknown_vectors():
    with open(unknown_path, 'rb') as f:
        unknown_vectors = pickle.load(f)
    return( unknown_vectors )


# In[7]:

import pickle 
unknown_path  = 'unknown_words_stored.pkl'


# In[9]:

w2v.save_unknown_vectors({"__BLANK__":np.array([0]*300+[1])})


# In[17]:

len(w2v.get_unknown_vectors())


# In[ ]:



