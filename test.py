from keras.models import *
from final_arch_utils import *

from bible_utils import *
import word2vec_utils as w2v
import numpy as np

bible_map = get_bible_map()
w2v_model = w2v.initialize()
w2v.unvectorize_initialize()

test_c, test_s, test_y = get_training_data(bible_map, w2v_model, 10)

style_transfer = load_model("style_transfer_best_unshackled.h5")
gen_y = style_transfer.predict([test_c, test_s])

for i in range(0, len(gen_y)):
    print "sentence", i
    print "target:", w2v.unvectorize_sentence(test_y[i])
    print "\n"
    print "generated:", w2v.unvectorize_sentence(gen_y[i])
    print "\n"
    print "\n\n"
