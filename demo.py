from keras.models import *
from final_arch_utils import *

from bible_utils import *
import word2vec_utils as w2v

import numpy as np

bible_map = get_bible_map()
w2v_model = w2v.initialize()
#unknowns = w2v.reset_global_unknown_vectors()

#print unknowns
#print "HI"

style_transfer = load_model("style_transfer.h5")

test_c, test_s, test_y = get_training_data(bible_map, w2v_model, 5)
gen_y = style_transfer.predict([test_c, test_s])

for g_y, y in zip(gen_y, test_y):
    gen_text = []
    src_text = []

    for gen_word in g_y:
        if gen_word[-1] == 1:
            gen_text.append("NULL")
        else:
            w = (w2v_model.similar_by_vector(gen_word[:-1]))[0]
            gen_text.append(w[0])

    for word in y:
        if word[-1] == 1:
            src_text.append("NULL")
        else:
            w = (w2v_model.similar_by_vector(word[:-1]))[0]
            src_text.append(w[0])

    print "target text"
    print " ".join(src_text)
    print "generated text"
    print " ".join(gen_text)

    sims = []

    for w1, w2 in zip(gen_text, src_text):
        if w1 != "NULL" and w2 != "NULL":
            sims.append(w2v_model.similarity(w1, w2))

    print "similarity scores"
    print sims

    print "\n\n"
