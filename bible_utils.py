import json

data_path = "data/"

def open_bible( bible_type ):
    return( open( data_path + bible_type + '.json'))

def get_bible_json( bible_type ):
    return( json.load( open_bible( bible_type )))

def format_changer( list_of_new_bibles ):
    map_versions = []
    for version in list_versions:
        dictionary_version = dict()
        for book in version:
            book_dictionary = dict()
            for chapter in book[ 'chapters' ]:
                book_dictionary[ chapter.keys()[0] ] = chapter[ chapter.keys()[0] ]
            dictionary_version[ book['book'] ] = book_dictionary
        map_versions.append(dictionary_version)
        json.dump( map_versions[1], open( 'data/' + version + '.json', 'w' ) )
    return( map_versions )


#bible_versions = ["AKJV","KJV","DBY","DRB","WEB","YLT","ASV","BBE",#came form getbible.net
#                 'ESV','MSG','NIV','NLT'] #came from https://github.com/honza/bibles

bible_versions = ['ESV', 'MSG', 'BBE', 'YLT', 'KJV', 'NLT', 'NIV']

def get_bible_map():
    bibles = map( get_bible_json, bible_versions )
    bible_map = dict(zip(bible_versions,bibles))
    return( bible_map )
bible_map = get_bible_map()
def get_verse( Book, Chapter, Verse ):
    ret = dict()
    for version,bible in bible_map.iteritems():
        ret[version] = bible[Book][str(Chapter)][str(Verse)].encode('ascii','ignore')
    return( ret )

# https://getbible.net/api is source for the below versions
def convert_getbible_net_bibles():
    versions = ["AKJV","KJV","DBY","DRB","WEB","WMB","YLT","ASV","BBE"]
    for version in versions:
        print version
        with open( "data/" + version + "_old.json", 'r+' ) as f:
            bible = json.load(f)['version']
            ret = {}
            i = 0
            for book_num in bible:
                book = {}
                book_name = bible[book_num]['book_name']
                book_content = bible[book_num]['book']
                for chapter_num in book_content:
                    chap = {}
                    chapter_content = book_content[chapter_num]['chapter']
                    for verse_num in chapter_content:
                        verse_content = chapter_content[verse_num]['verse']
                        chap[verse_num] = verse_content
                    book[chapter_num] = chap

                ret[book_name] = book
            with open( "data/" + version + ".json", 'w+' ) as f:
                f.seek(0)
                f.truncate()
                json.dump(ret,f)
