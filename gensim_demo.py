import gensim
import re

# you'll have to download the word2vec file yourself as it's too big to put on bitbucket
# https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit
word2vec_path = './GoogleNews-vectors-negative300.bin'
model = gensim.models.KeyedVectors.load_word2vec_format(word2vec_path, binary=True)

# test that the word2evc model is working: this famous example should print the vector for queen
print model.most_similar(positive=['woman', 'king'], negative=['man'])

### Preprocessing Example
# example sentence
s = "This is a test string with some stuff: John's, hello! Hello!"

# insert a space before all punctuation except apostrophe
s = re.sub(r"([\w/'+$\s-]+|[^\w/'+$\s-])\s*", r" \1", s)
# tokenize
line = str.split(s.strip(), " ")
# vectorize
for word in line:
    if word in model:
        print model[word]
    else:
        print "Out of Vocabulary:", word
