from keras.layers import Input, LSTM, RepeatVector, concatenate
from keras.models import Model, Sequential
from keras import optimizers
from keras import backend as K
from final_arch_utils import *
#from recurrentshop import *

from bible_utils import *
import word2vec_utils as w2v

K.set_learning_phase(1)
K._LEARNING_PHASE = K.constant(1)

bible_map = get_bible_map()
w2v_model = w2v.initialize()

# size definitions
embedding_size = 301
latent_dim = 301
content_max_length = 50
style_max_length = 150
output_size = 50

nb_epoch = 3
train_flag = True

# define shapes of the inputs
contentInput = Input(shape=(content_max_length, embedding_size)) # 50 size 301 word vectors
styleInput = Input(shape=(style_max_length, embedding_size)) # 150 size 301 word vectors

# define content model
content_l1 = LSTM(latent_dim, return_sequences = True, dropout=0.15, recurrent_dropout=0.15, name="content_1")
content_l2 = LSTM(latent_dim, return_sequences = True, dropout=0.15, recurrent_dropout=0.15, name="content_2")
content_l3 = LSTM(latent_dim, return_sequences = True, dropout=0.15, recurrent_dropout=0.15, name="content_3")

# freeze content weights for first few epochs
content_l1.trainable = train_flag
content_l2.trainable = train_flag
content_l3.trainable = train_flag

content = content_l3(content_l2(content_l1(contentInput)))

# load content model's weights from pre-training
content_model = Model(contentInput, content)
#content_model.load_weights("content_weights.h5", by_name=True)

# define style model
style_l1 = LSTM(latent_dim, return_sequences = True, dropout=0.2, recurrent_dropout=0.2, name="style_1")
style_l2 = LSTM(latent_dim-101, return_sequences = True, dropout=0.2, recurrent_dropout=0.2, name="style_2")
style_l3 = LSTM(latent_dim-201, return_sequences = False, dropout=0.2, recurrent_dropout=0.2, name="style_3")

# freeze style weights for first few epochs
style_l1.trainable = train_flag
style_l2.trainable = train_flag
style_l3.trainable = train_flag

style = style_l3(style_l2(style_l1(styleInput)))

# load style model's weights from pre-training
style_model = Model(styleInput, style)
#style_model.load_weights("style_weights.h5", by_name=True)

# define final style transfer model
style = RepeatVector(output_size)(style)
latent = concatenate([content, style])
#latent = concatenate([content, style])
#synthesized = RepeatVector(output_size)(synthesized)
synthesized = LSTM(embedding_size, return_sequences = True)(latent)
synthesized = LSTM(embedding_size, return_sequences = True)(synthesized)
synthesized = LSTM(embedding_size, return_sequences = True)(synthesized)

#expander = RecurrentSequential(decode=True, output_length=output_size)
#expander.add(LSTMCell(embedding_size, input_dim=latent_dim + (latent_dim - 201)))
#expander.add(LSTMCell(embedding_size))
#expander.add(LSTMCell(embedding_size))

#synthesized = expander(latent)

# final model
style_transfer = Model(inputs=[contentInput, styleInput], outputs=[synthesized])

style_transfer.load_weights("style_transfer.h5", by_name=True)

#adam = optimizers.Adam(lr=0.0005, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
rmsprop = optimizers.RMSprop(lr=0.0007, rho=0.9, epsilon=1e-08, decay=0.0)
style_transfer.compile(optimizer=rmsprop, loss="cosine_proximity", metrics=["accuracy"])

style_transfer.summary()

#test_c, test_s, test_y = get_training_data(bible_map, w2v_model, 10)
#gen_y = style_transfer.predict([test_c, test_s])

# load training data
train_c, train_s, train_y = get_training_data(bible_map, w2v_model, 5000)

print "finished creating dataset 1"

# train the model for realsies
#K.set_learning_phase(1)
#print(K.learning_phase())
style_transfer.fit([train_c, train_s], train_y, validation_split=0.05, epochs=nb_epoch)

train_c, train_s, train_y = get_training_data(bible_map, w2v_model, 5000)
print "finished creating dataset 2"
style_transfer.fit([train_c, train_s], train_y, validation_split=0.05, epochs=nb_epoch)

#train_c, train_s, train_y = get_training_data(bible_map, w2v_model, 5000)
#print "finished creating dataset 3"
#style_transfer.fit([train_c, train_s], train_y, validation_split=0.05, epochs=nb_epoch)

#train_c, train_s, train_y = get_training_data(bible_map, w2v_model, 5000)
#print "finished creating dataset 4"
#style_transfer.fit([train_c, train_s], train_y, validation_split=0.05, epochs=nb_epoch)


#test_c, test_s, test_y = get_training_data(bible_map, w2v_model, 1000)

# test the model
#style_transfer.evaluate([test_c, test_s], test_y)

style_transfer.save("style_transfer.h5")
